//
//  GuestTableViewCell.swift
//  Mid-Project
//
//  Created by KiD on 11/15/19.
//  Copyright © 2019 KiD. All rights reserved.
//

import UIKit

class GuestTableViewCell: UITableViewCell{
    
    
    @IBOutlet weak var guestName: UILabel!
    
    @IBOutlet weak var section: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
