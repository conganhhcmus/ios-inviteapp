//
//  AdminController.swift
//  Mid-Project
//
//  Created by KiD on 11/13/19.
//  Copyright © 2019 KiD. All rights reserved.
//

import UIKit
import RealmSwift

class AdminController: UIViewController {

    
    
    
    @IBAction func CreateEvent(_ sender: Any) {
        //alert
        let alertFont = UIAlertController(title: "Create New Event", message: "Create new is delete all the info from previous or current event in the app !", preferredStyle: .alert)
        let src = storyboard?.instantiateViewController(identifier: "event") as! EventController
        src.modalPresentationStyle = .fullScreen
    
    alertFont.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alertFont.addAction(UIAlertAction(title: "OK", style: .default, handler: {(UIAlertAction) in self.deleEventData(); self.present(src, animated: true,completion: nil)}))
        self.present(alertFont,animated: true,completion: nil)
    }
    
    func deleEventData() {
        let realm = try! Realm()
        if realm.objects(EventData.self).count > 0 {
            let users = realm.objects(EventData.self)
            for u in users {
                try! realm.write {
                    realm.delete(u)
                }
            }
        }
        if realm.objects(GuestData.self).count > 0 {
            let users = realm.objects(GuestData.self)
            for u in users{
                try! realm.write {
                    realm.delete(u)
                }
            }
        }
    }
    
    @IBOutlet weak var btnCreate: UIButton!
    
    @IBOutlet weak var btnEdit: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        btnCreate.layer.cornerRadius = 10
        btnCreate.layer.borderWidth = 2
        btnCreate.titleLabel?.numberOfLines = 2
        btnCreate.titleLabel?.textAlignment = NSTextAlignment.center
        
        btnEdit.layer.cornerRadius = 10
        btnEdit.layer.borderWidth = 2
        btnEdit.titleLabel?.numberOfLines = 2
        btnEdit.titleLabel?.textAlignment = NSTextAlignment.center
    }
    

}
