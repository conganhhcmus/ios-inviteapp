//
//  AddGuestController.swift
//  Mid-Project
//
//  Created by KiD on 11/14/19.
//  Copyright © 2019 KiD. All rights reserved.
//

import UIKit
import RealmSwift

class AddGuestController: UIViewController, UITextFieldDelegate{
    
    var guestData = GuestData()
    
    @IBOutlet weak var firstName: UITextField!
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
    }
    
    @IBAction func GetFirstName(_ sender: Any) {
        // lay first name
        guestData.guestFirstName = firstName.text!
        
    }
    
    @IBAction func GetLastName(_ sender: Any) {
        guestData.guestLastName = lastName.text!
    }
    
    @IBAction func GetGuest(_ sender: Any) {
        guestData.guestGuests = guest.text!
    }
    
    
    @IBAction func GetTable(_ sender: Any) {
        guestData.guestTable = table.text!
    }
    
    @IBAction func GetSection(_ sender: Any) {
        guestData.guestSection = section.text!
    }
    
    func saveGuest() {
        let realm = try! Realm()
        do {
            try realm.write {
                
                realm.add(guestData)
                
            //print(Realm.Configuration.defaultConfiguration.fileURL)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    @IBAction func Save(_ sender: Any) {
        saveGuest()
    }
    
    @IBOutlet weak var lastName: UITextField!
    
    @IBOutlet weak var guest: UITextField!
    
    @IBOutlet weak var table: UITextField!
    
    @IBOutlet weak var section: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        firstName.delegate = self
        lastName.delegate = self
        guest.delegate = self
        table.delegate = self
        section.delegate = self
        // Do any additional setup after loading the view.
    }
    

 

}
