//
//  EventController.swift
//  Mid-Project
//
//  Created by KiD on 11/13/19.
//  Copyright © 2019 KiD. All rights reserved.
//

import UIKit
import RealmSwift

class EventController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource,UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var tv: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          let realm = try! Realm()
          
          let guests = realm.objects(GuestData.self)
          return guests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let realm = try! Realm()
        
        let guests = realm.objects(GuestData.self)
        let cell = tv.dequeueReusableCell(withIdentifier: "Cells", for: indexPath) as! TableViewCell
        
        cell.lblName.text = guests[indexPath.row].guestFirstName + ", " + guests[indexPath.row].guestLastName
        cell.lblGuest.text = guests[indexPath.row].guestGuests
        cell.lblTable.text = guests[indexPath.row].guestTable
        cell.lblSector.text = guests[indexPath.row].guestSection
        
        return cell
    }
    
    
    var eventData = EventData()
    var save = false
    
    
    @IBOutlet weak var tableView: UITableView!
    
    
    
    
    @IBAction func Save(_ sender: Any) {  
        //deleEventData()
        
        if(!save) {
            //print("save")
            saveData()
            save = !save
        } else {
            //print("update")
            updateEventData()
        }

    }

    
    func loadEventData() {
        
        let realm = try! Realm()
        
        //print(Realm.Configuration.defaultConfiguration.fileURL)
        
        let users = realm.objects(EventData.self)
        if users.count > 0 {
            save = true
        }
        for u in users{
            eventName.text = u.eventName
            curFont.text = u.eventFont
            curFontSize.text = u.eventFontSize
            fontSizeSlider.value = Float(u.eventFontSize)!
            curFont.font = UIFont(name: curFont.text ?? listFont[0], size: 17)
        }
    }
    
    func deleEventData() {
        let realm = try! Realm()
        if realm.objects(EventData.self).count > 0 {
        let users = realm.objects(EventData.self)
        for u in users{
            try! realm.write {
                realm.delete(u)
            }
        }
        }
    }
    
    func updateEventData() {
        let realm = try! Realm()
        if realm.objects(EventData.self).count > 0 {
            let users = realm.objects(EventData.self)
            //let guest = realm.objects(GuestData.self)
            try! realm.write {
                //users.first?.number = guest.count
                users.first?.eventName = eventName.text!
                users.first?.eventFont = curFont.text!
                users.first?.eventFontSize = curFontSize.text!
            }
        }
    }
    
    func saveData() {
        
        self.eventData.eventName = eventName.text!
        self.eventData.eventFont = curFont.text!
        self.eventData.eventFontSize = curFontSize.text!
        
        let realm = try! Realm()

        do {
            try realm.write {
                realm.add(eventData)
            }
        }
        catch {
            print(error.localizedDescription)
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return listFont.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return listFont[row]
    }
    
    var font_Current: String = ""
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        // lưu dữ liệu
        font_Current = listFont[row]
    }
    

    var listFont: [String] = UIFont.familyNames.sorted()
    
    @IBOutlet weak var curFont: UILabel!
    
    @IBOutlet weak var fontSizeSlider: UISlider!
    
    @IBOutlet weak var curFontSize: UILabel!
    
    @IBOutlet weak var btnFont: UIButton!
    
    @IBAction func ChooseFont(_ sender: Any) {
        let alertFont = UIAlertController(title: "", message: "\n\n\n\n\n\n", preferredStyle: .alert)
        
        let pickerFont = UIPickerView(frame: CGRect(x: 5, y: 20, width: 250, height: 140))
        
        pickerFont.delegate = self
        pickerFont.dataSource = self
        
        alertFont.view.addSubview(pickerFont)
        
        alertFont.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alertFont.addAction(UIAlertAction(title: "OK", style: .default, handler: {(UIAlertAction) in self.curFont.text = self.font_Current; self.curFont.font = UIFont(name: self.font_Current, size: 17)}))
        
        
        self.present(alertFont,animated: true,completion: nil)
        
    }
        
    
    
    
    @IBOutlet weak var eventName: UITextField!
    
    @IBAction func setEvent(_ sender: Any) {
        // lưu tên event gõ vào
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.curFontSize.text = String(fontSizeSlider.value)
        eventName.delegate = self
        
        loadEventData()
        
        //curFont.text = listFont[0]
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
    }
    
    @IBAction func changeFontSize(_ sender: Any) {
        self.curFontSize.text = String(fontSizeSlider.value)
    }
    

    
    
}
