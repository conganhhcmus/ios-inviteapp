//
//  FindViewController.swift
//  Mid-Project
//
//  Created by KiD on 11/14/19.
//  Copyright © 2019 KiD. All rights reserved.
//

import UIKit
import RealmSwift

class FindViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource,UITableViewDataSource,UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countGuest
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "GuestCell", for: indexPath) as! GuestTableViewCell
        if (countGuest > 0) {
        cell.guestName.text = guests[indexPath.row].guestFirstName + ", " + guests[indexPath.row].guestLastName
        cell.section.text = guests[indexPath.row].guestSection
        }
        return cell
    }
    
    
    
    @IBOutlet weak var table: UILabel!
    
    
    @IBOutlet weak var section: UILabel!
    
    var arr:[GuestData] = []
    var guests:[GuestData] = []
    var count:Int = 0
    var countGuest:Int = 0
    var cur_table:String = ""
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func CountGuest(){
        guests.removeAll()
        let realm = try! Realm()
               let users = realm.objects(GuestData.self)
               
               for u in users {
                   if u.guestTable.contains(cur_table) {
                       guests.append(u)
                   }
               }
               countGuest = guests.count
    }
    
    func CountName() {
        
        let realm = try! Realm()
        let users = realm.objects(GuestData.self)
        
        for u in users {
            if u.guestFirstName.contains(GuestController.findName) || u.guestLastName.contains(GuestController.findName) {
                arr.append(u)
            }
        }
        count = arr.count
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arr[row].guestFirstName + ", " + arr[row].guestLastName
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        // lưu dữ liệu
        if count > 0 {
            table.text = arr[row].guestTable
            section.text = arr[row].guestSection
            cur_table = arr[row].guestTable
            CountGuest()
            tableView.reloadData()
        }
    }

    @IBOutlet weak var selectName: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CountName()
        if count > 0 {
        table.text = arr[0].guestTable
        cur_table = arr[0].guestTable
        section.text = arr[0].guestSection
        CountGuest()
        print(countGuest)
        }
       
    }

}
