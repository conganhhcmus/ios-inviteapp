//
//  TableViewCell.swift
//  Mid-Project
//
//  Created by KiD on 11/14/19.
//  Copyright © 2019 KiD. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblGuest: UILabel!
    @IBOutlet weak var lblTable: UILabel!
    @IBOutlet weak var lblSector: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
