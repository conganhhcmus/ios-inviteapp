//
//  GuestController.swift
//  Mid-Project
//
//  Created by KiD on 11/13/19.
//  Copyright © 2019 KiD. All rights reserved.
//

import UIKit
import RealmSwift

class GuestController: UIViewController,UITextFieldDelegate {
    
    static var findName:String = ""

    @IBOutlet weak var findMyTable: UIButton!
    
    @IBOutlet weak var yourName: UITextField!
    
    @IBAction func SetFindName(_ sender: Any) {
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
        GuestController.findName = yourName.text ?? ""
        return self.view.endEditing(true)
           
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        findMyTable.layer.cornerRadius = 10
        findMyTable.layer.borderWidth = 2
        findMyTable.titleLabel?.numberOfLines = 1
        findMyTable.titleLabel?.textAlignment = NSTextAlignment.center
        
        yourName.delegate = self
        
    }
    


}
