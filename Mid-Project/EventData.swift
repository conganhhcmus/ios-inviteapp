//
//  EventData.swift
//  Mid-Project
//
//  Created by KiD on 11/14/19.
//  Copyright © 2019 KiD. All rights reserved.
//

import Foundation
import RealmSwift

class EventData:Object {
    //@objc dynamic var number:Int = 0
    @objc dynamic var eventName:String = ""
    @objc dynamic var eventFont:String = ""
    @objc dynamic var eventFontSize:String = ""
    @objc dynamic var eventFontColor:String = ""
    
}

class GuestData:Object {
    @objc dynamic var guestFirstName:String = ""
    @objc dynamic var guestLastName:String = ""
    @objc dynamic var guestGuests:String = ""
    @objc dynamic var guestTable:String = ""
    @objc dynamic var guestSection:String = ""
}
